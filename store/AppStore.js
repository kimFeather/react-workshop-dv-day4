import { createStore, combineReducers} from 'redux'
import ImgReducer from '../reducers/ImgReducer'
import UserInfoReducer from '../reducers/UserInfoReducer'

const reducers = combineReducers({   
    images: ImgReducer,
    userInfo: UserInfoReducer
});

const store = createStore(reducers);
const state = store.getState();
export default store;