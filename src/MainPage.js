import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView, TouchableOpacity ,FlatList } from 'react-native';
import { Link } from 'react-router-native';


export default class MainPage extends React.Component {

    state = {
        username: '',
        password: '',
        name: '',
        surname: '',
        dataLeft: [{
            key: '1',
            pname: 'Lorem1'
        }, {
            key: '3',
            pname: 'Lorem3'
        }, {
            key: '5',
            pname: 'Lorem5'
        }, {
            key: '7',
            pname: 'Lorem7'
        }],
        dataRight: [{
            key: '2',
            pname: 'Lorem2'
        }, {
            key: '4',
            pname: 'Lorem4'
        }, {
            key: '6',
            pname: 'Lorem6'
        }, {
            key: '8',
            pname: 'Lorem8'
        }],
        productClick: { key: '', pname: '' }
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.username && this.props.location.state.password) {
            this.setState({ username: this.props.location.state.username })
            this.setState({ password: this.props.location.state.password })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.name && this.props.location.state.surname) {
            this.setState({ name: this.props.location.state.name })
            this.setState({ surname: this.props.location.state.surname })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.dataLeft && this.props.location.state.dataRight) {
            this.setState({ dataLeft: this.props.location.state.dataLeft })
            this.setState({ dataRight: this.props.location.state.dataRight })
        }
    }

    logout = () => {
        this.props.history.replace('/')
    }

    navigateToAddPage = () => {
        this.props.history.replace('/addproduct', this.state)
    }

    navigateToProfilePage = () => {
        this.props.history.replace('/profile', this.state)
    }

    clickProduct = (product) => {
        console.log(product);
        const tempState = this.state
        tempState.productClick = product
        this.props.history.replace('/product', tempState)
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <View style={styles.header}>
                        <Text style={{ fontSize: 24, color: 'white' }}>Main Page</Text>
                    </View>
                </View>
                <ScrollView style={styles.scrollContainer}>
                    <View style={styles.scrollContent}>
                        <View style={styles.contentChild}>
                            <FlatList
                                data={this.state.dataLeft}
                                renderItem={(item) => (
                                    <TouchableOpacity style={styles.box} onPress={() => this.clickProduct(item.item)}>
                                        <Text style={styles.boxText}>{item.item.pname}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                        <View style={styles.contentChild}>
                            <FlatList
                                data={this.state.dataRight}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={styles.box} onPress={() => { this.clickProduct(item) }}>
                                        <Text style={styles.boxText}>{item.pname}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={this.logout} style={[styles.squareButton]}>
                        <Text style={{ color: 'white', fontSize: 25 }}>L</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.navigateToAddPage} style={styles.header}>
                        <Text style={{ fontSize: 24, color: 'white' }}>Add</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.squareButton]} onPress={this.navigateToProfilePage}>
                        <Text style={{ color: 'white', fontSize: 25 }}>P</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerSection: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: '#8b9dc3',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: '#3b5998',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContainer: {
        backgroundColor: '#dfe3ee',
        flex: 1
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    contentChild: {
        flex: 1,
    },
    box: {
        height: 180,
        backgroundColor: '#ffffff',
        margin: 10
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    }
})








