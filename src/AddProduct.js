import React from 'react';
import { StyleSheet, Text, TextInput, View, ScrollView, Image, Button, TouchableOpacity } from 'react-native';

export default class AddProduct extends React.Component {

    state = {
        username: '',
        password: '',
        name: '',
        surname: '',
        dataLeft: [],
        dataRight: [],
        newProductName: ''
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.username && this.props.location.state.password) {
            this.setState({ username: this.props.location.state.username })
            this.setState({ password: this.props.location.state.password })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.name && this.props.location.state.surname) {
            this.setState({ name: this.props.location.state.name })
            this.setState({ surname: this.props.location.state.surname })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.dataLeft && this.props.location.state.dataRight) {
            this.setState({ dataLeft: this.props.location.state.dataLeft })
            this.setState({ dataRight: this.props.location.state.dataRight })
        }
    }

    navigateToMainPage = () => {
        this.setState({ dataLeft: this.props.location.state.dataLeft })
        this.setState({ dataRight: this.props.location.state.dataRight })
        this.props.history.replace('/main', this.state)
    }

    saveInformation = () => {
        if (this.state.newProductName !== '') {
            if (this.state.dataLeft !== [] && this.state.dataRight !== []) {
                const newId = (this.state.dataLeft.length + this.state.dataRight.length + 1).toString()
                const productLeft = this.state.dataLeft
                const productRight = this.state.dataRight
                const newProduct = {
                    key: newId,
                    pname: this.state.newProductName
                }
                if (productRight.length < productLeft.length) {
                    productRight.push(newProduct)
                    this.setState({ dataLeft: productLeft })
                } else {
                    productLeft.push(newProduct)
                    this.setState({ dataRight: productRight })
                }
            }
            this.props.history.replace('/main', this.state)
        } else alert('Cannot Leave Product Name Empty')
    }

    changeText = (text) => {
        this.setState({ newProductName: text })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={[styles.squareButton]} onPress={this.navigateToMainPage}>
                        <Text style={{ color: 'white', fontSize: 25 }}>X</Text>
                    </TouchableOpacity>
                    <View style={styles.headerText}>
                        <Text style={{ fontSize: 24, color: 'white' }}>Add Product</Text>
                    </View>
                </View>
                <View style={styles.scrollContainer}>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Image (Comming Soon)"
                        value=''
                        onChangeText={(value) => {
                            //TODO: Provide Image upload function using AntD ; Maybe later
                        }}
                    />
                    <TextInput style={styles.textInput}
                        placeholder="Product Name"
                        value={this.state.newProductName}
                        onChangeText={(value) => { this.changeText(value) }}
                    />
                    <TouchableOpacity
                        style={{
                            width: 140,
                            height: 40,
                            backgroundColor: 'gray',
                            marginTop: 40,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={this.saveInformation}
                    >
                        <Text style={{ color: 'white', fontSize: 20 }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: '#8b9dc3',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        flex: 1,
        backgroundColor: '#3b5998',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContainer: {
        backgroundColor: '#dfe3ee',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    textInput: {
        width: 350,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#c2c8d1',
        marginBottom: 20,
        fontSize: 20
    },
    contentChild: {
        flex: 1,
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    }
})
