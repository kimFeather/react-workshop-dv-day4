import React from 'react';
import { StyleSheet, Text, TextInput, View, ScrollView, Image, Button, TouchableOpacity } from 'react-native';

export default class EditProfilePage extends React.Component {

    state = {
        username: '',
        password: '',
        name: '',
        surname: '',
        dataLeft: [],
        dataRight: []
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.username && this.props.location.state.password) {
            this.setState({ username: this.props.location.state.username })
            this.setState({ password: this.props.location.state.password })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.name && this.props.location.state.surname) {
            this.setState({ name: this.props.location.state.name })
            this.setState({ surname: this.props.location.state.surname })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.dataLeft && this.props.location.state.dataRight) {
            this.setState({ dataLeft: this.props.location.state.dataLeft })
            this.setState({ dataRight: this.props.location.state.dataRight })
        }
    }

    navigateToProfilePage = () => {
        this.setState({ name: this.props.location.state.name })
        this.setState({ surname: this.props.location.state.surname })
        this.props.history.replace('/profile', this.state)
    }

    saveInformation = () => {
        this.props.history.replace('/profile', this.state)
    }

    changeText = (field, text) => {
        this.setState({ [field]: text })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <TouchableOpacity style={[styles.squareButton]} onPress={this.navigateToProfilePage}>
                        <Text style={{ color: 'white', fontSize: 25 }}>X</Text>
                    </TouchableOpacity>
                    <View style={styles.headerText}>
                        <Text style={{ fontSize: 24, color: 'white' }}>Edit Profile Page</Text>
                    </View>
                </View>
                <View style={styles.scrollContainer}>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Name"
                        value={this.state.name}
                        onChangeText={(value) => { this.changeText('name', value) }}
                    />
                    <TextInput style={styles.textInput}
                        placeholder="Surname"
                        value={this.state.surname}
                        onChangeText={(value) => { this.changeText('surname', value) }}
                    />
                    <TouchableOpacity
                        style={{
                            width: 140,
                            height: 40,
                            backgroundColor: 'gray',
                            marginTop: 40,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={this.saveInformation}
                    >
                        <Text style={{ color: 'white', fontSize: 20 }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerSection: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: '#8b9dc3',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        flex: 1,
        backgroundColor: '#3b5998',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContainer: {
        backgroundColor: '#dfe3ee',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    textInput: {
        width: 350,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#c2c8d1',
        marginBottom: 20,
        fontSize: 20
    },
    contentChild: {
        flex: 1,
        // padding: 14
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    }
})
