import {createStore , combineReducers} from 'redux';
import TodosReducer from './TodosReducer';
import CompleteReducer from './CompletesReducers';

const reducers = combineReducers({           //รับตัวแปร obj
    todos : TodosReducer,
    completes : CompleteReducer
})

const store = createStore(TodosReducer);

export default store;