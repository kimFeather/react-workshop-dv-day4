export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_IMAGE':
            return [...state, {
                image: action.image,
                imageName: action.imageName,
                toggle: false
            }]
        case 'TOGGLE_IMAGE':
            return state.map(
                (item, index) => {
                    return index === action.index ? !item.toggle : item.toggle
                }
            )
        default:
            return state
    }
}
