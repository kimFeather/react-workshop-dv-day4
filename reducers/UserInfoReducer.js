export default (state = [], action) => {
    switch (action.type) {
        case 'USER_LOGIN':
        console.log('login action:', action)
            return [...state, {
                username: action.username,
                password: action.password,
                name: action.name,
                lastname: action.surname,
                
            }]
            case 'USER_EDIT':
            return {
                ...state,
                name: action.name,
                surname: action.surname
            }
        case 'USER_LOGOUT':
            return {}
        default:
            return state
    }
}
